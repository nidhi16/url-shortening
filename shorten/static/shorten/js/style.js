$(document).ready(function() {
    $('form').submit(function(event) {
        event.preventDefault();
        $('div#prog').css('visibility','visible');
        var formData;
        formData = {
            'url': $('input[name=url]').val()
        };

        $.ajax({
            type: 'POST',
            url: 'submit/',
            data: formData
        }).fail(function() {
            console.log("Request Failed");
        }).done(function(data) {
            $('div#prog').css('visibility','hidden');
            console.log("Always Executed");
            console.log(data);
            document.getElementById('demo').innerHTML = '<a href="' + data + '">' + data + '</a>';
        });
    });
});
