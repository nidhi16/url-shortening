from django.conf.urls import url
from . import views

app_name = 'ushort'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^submit/$', views.submit, name='submit'),
    url(r'^(?P<url_id>[0-9]+)/create/$', views.create, name='create'),
    url(r'^(?P<url_id>[0-9]+)/$', views.redirect, name='redirect'),
]
