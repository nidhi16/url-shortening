from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Urls
from django.core.urlresolvers import reverse
from django.contrib.sites.shortcuts import get_current_site

# Create your views here.


def index(request):
    return render(request, 'ushort/index.html')


def submit(request):
    long_url = request.POST.get('url')
    q = Urls(original_url=long_url)
    q.save()
    url_id = q.id
    return HttpResponseRedirect(reverse('ushort:create', args=(url_id,)))


def create(request, url_id):
    prev_url = Urls.objects.get(id=url_id)
    url_id = prev_url.id
    current_domain = get_current_site(request)
    if request.is_secure():
        our_protocol = "https://"
    else:
        our_protocol = "http://"
    current_url = our_protocol + str(current_domain) + reverse('ushort:redirect', args=(url_id,))
    context = {
        'current_url': current_url,
        'url_id': url_id
    }
    return render(request, 'ushort/create.html', context)


def redirect(request, url_id):
    url = Urls.objects.get(id=url_id)
    return HttpResponseRedirect(url.original_url)
