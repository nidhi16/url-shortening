from __future__ import unicode_literals

from django.apps import AppConfig


class JsontestConfig(AppConfig):
    name = 'jsonTest'
