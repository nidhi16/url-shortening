from django.conf.urls import url
from . import views

app_name = 'jsonTest'

urlpatterns = [
    url(r'^$', views.index, name='index')
]