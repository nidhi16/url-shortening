from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
# Create your views here.



def index(request):
    response = {
        'hello': 'world',
        'foo': 'bar',
    }
    return HttpResponse(json.dumps(response), content_type='application/json', status=200)
    # return JsonResponse(response)
