from django.http import HttpResponseRedirect
from django.shortcuts import render
from ushort.models import Urls
from django.core.urlresolvers import reverse
from django.contrib.sites.shortcuts import get_current_site
# Create your views here.


def index(request):
    return render(request, 'short/index.html')


def submit(request):
    long_url = request.POST.get('url')
    q = Urls(original_url=long_url)
    q.save()
    url_id = q.id
    current_domain = get_current_site(request)
    new_url = "http://" + str(current_domain) + reverse('short:redirect', args=(url_id,))
    context = {
        'new_url': new_url,
    }
    return render(request, 'short/submit.html', context)


def redirect(request, url_id):
    url = Urls.objects.get(id=url_id)
    return HttpResponseRedirect(url.original_url)